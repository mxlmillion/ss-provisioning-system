package main

import (
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/streadway/amqp"
	"log"
	"net/http"
	"ss-lib/configuration"
	"ss-lib/messagequeue"
	mq_ampq "ss-lib/messagequeue/amqp"
	"ss-lib/mqcontracts"
	"ss-lib/persistence"
	"ss-lib/persistence/dblayer"
	"strings"
	"time"
)

func main() {
    // extract configuration
	confPath := flag.String("conf", `.\configuration\config.json`, "flag to set the path to the configuration json file")
    flag.Parse()
    config, _ := configuration.ExtractConfiguration(*confPath)

    // connect to DB
    fmt.Println("Connecting to database")
    dbh, err := dblayer.NewPersistenceLayer(config.Databasetype, config.DBConnection)
    if err != nil {
    	fmt.Printf("Error connecting to DB: %s\n", err)
    }

    // connect to ampq provider
    amqpconnstr := "amqp://" + config.AmqpUser + ":" + config.AmqpUserPw + "@" + config.AmqpHostname + ":" + config.AmqpPort
    mqconn, err := amqp.Dial(amqpconnstr)
    if err != nil {
    	fmt.Printf("Error connecting to AMPQ provider: %s\n", err)
    }
    fmt.Printf("AMQP provider connected at: '%s'\n", amqpconnstr)
    emitter, err := mq_ampq.NewAMQPEventEmitter(mqconn)
    if err != nil {
    	panic(err)
    }
    
    
    //RESTful API start
    //log.Fatal(rest.ServeAPI(config.RestfulEndpoint, dbhandler, eventEmitter))
	apierrchan, tlsapierrchan := StartApi(config.TLSListen, config.TLSCert, config.TLSKey, config.RestfulEndpoint, dbh, emitter)
	select {
		case newerr := <-apierrchan:
			log.Printf("got an http server error: %s\n", newerr)
		case tlserr := <-tlsapierrchan:
			log.Printf("got an https (TLS) server error: %s\n", tlserr)
	}
}

func StartApi(tlslisten, cert, key, listenstr string, db persistence.DatabaseHandler, eventemitter messagequeue.EventEmitter) (chan error, chan error) {
	r := mux.NewRouter()
	handler := newEventHandler(db, eventemitter)
	eventsrouter := r.PathPrefix("/events").Subrouter()
	eventsrouter.Methods("GET").Path("/{SearchCriteria}/{Search}").HandlerFunc(handler.findEventHandler)
	eventsrouter.Methods("GET").Path("").HandlerFunc(handler.allEventHandler)
	eventsrouter.Methods("POST").Path("").HandlerFunc(handler.newEventHandler)

	errchan := make(chan error)
	tlserrchan := make(chan error)
	go func() { errchan <- http.ListenAndServe(listenstr, r) }()
	fmt.Printf("Listening HTTP on %s\n", listenstr)
	go func() { tlserrchan <- http.ListenAndServeTLS(tlslisten, cert, key, r)}()
	fmt.Printf("Listening HTTPS on %s\n", tlslisten)
	return errchan, tlserrchan
}

func newEventHandler(dbh persistence.DatabaseHandler, e messagequeue.EventEmitter) *eventServiceHandler {
	return &eventServiceHandler{
		dbhandler: dbh,
		eventemitter: e,
	}
}

type eventServiceHandler struct {
	dbhandler persistence.DatabaseHandler
	eventemitter messagequeue.EventEmitter
}
func (eh *eventServiceHandler) findEventHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	searchcriteria, ok := vars["SearchCriteria"]
	if ! ok {
		w.WriteHeader(400)
		fmt.Fprint(w, `{error: no SearchCriteria supplied: /events/???}`)
		return
	}
	search, ok := vars["Search"]
	if ! ok {
		w.WriteHeader(400)
		fmt.Fprint(w, `{error: no Search supplied: /events/\{SearchCriteria\}/???}`)
		return
	}

	var err error
	var event persistence.Event
	switch strings.ToLower(searchcriteria) {
	case "name":
		event, err = eh.dbhandler.FindEventByName(search)
	case "id":
		id, err := hex.DecodeString(search)
		if err == nil {
            event, err = eh.dbhandler.FindEvent(id)
        }
	}

	if err != nil {
		w.WriteHeader(404)
		fmt.Fprintf(w, "{error: %s}", err)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf8")
    json.NewEncoder(w).Encode(&event)

}
func (eh *eventServiceHandler) allEventHandler(w http.ResponseWriter, r *http.Request) {
	events, err := eh.dbhandler.FindAllAvailableEvents()
	if err != nil {
		w.WriteHeader(500)
		fmt.Fprintf(w, "{error: Error while retrieving all events: %s}", err)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf8")
	err = json.NewEncoder(w).Encode(events)
	if err != nil {
		w.WriteHeader(500)
		fmt.Fprintf(w, "{error: Error while encoding results to json: %s}", err)
		return
	}

}
func (eh *eventServiceHandler) newEventHandler(w http.ResponseWriter, r *http.Request) {
	event := persistence.Event{}
    err := json.NewDecoder(r.Body).Decode(&event)
    if err != nil {
        w.WriteHeader(500)
        fmt.Fprintf(w, "{error: error occured while decoding event data %s}", err)
        return
    }

    id, err := eh.dbhandler.AddEvent(event)
    if nil != err {
        w.WriteHeader(500)
        fmt.Fprintf(w, "{error: error occured while persisting event %d %s}",id, err)
        return
    }

    msg := mqcontracts.EventCreatedEvent{ 
		ID: hex.EncodeToString(id), 
		Name: event.Name,
		Start: time.Unix(event.StartDate, 0), 
		End: time.Unix(event.EndDate, 0), 
	}
	eh.eventemitter.Emit(&msg)
}
