package main 
 
import (
	"fmt"
	"github.com/streadway/amqp"
	"ss-lib/persistence/dblayer"
	"ss-provisioner/rest"
"ss-lib/configuration"
msgqueue_amqp "ss-lib/messagequeue/amqp"
"ss-provisioner/listener"
"flag"
)
 
func main() { 
	confPath := flag.String("config", "./configuration/config.json", "path to config file") 
	flag.Parse() 
	config, _ := configuration.ExtractConfiguration(*confPath)

	db, err := dblayer.NewPersistenceLayer(config.Databasetype, config.DBConnection)
	if err != nil { 
		panic(err) 
	}

	amqpconnstr := "amqp://" + config.AmqpUser + ":" + config.AmqpUserPw + "@" + config.AmqpHostname + ":" + config.AmqpPort
	conn, err := amqp.Dial(amqpconnstr)
	if err != nil { 
		panic(err) 
	}
	fmt.Printf("AMQP provider connected at: '%s'\n", amqpconnstr)

	eventListener, err := msgqueue_amqp.NewAMQPEventListener(conn, "events", "provisioning")
	if err != nil { 
		panic(err) 
	}

	processor := &listener.EventProcessor{eventListener, dblayer} 
	go processor.ProcessEvents() 

	rest.StartAPI(config.RestfulEndpoint, db, eventEmitter)
} 