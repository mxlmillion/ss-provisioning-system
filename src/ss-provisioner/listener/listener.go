package listener

import (
	"log"
	"ss-lib/mqcontracts"
)
import "ss-lib/messagequeue" 
import "ss-lib/persistence" 
import "gopkg.in/mgo.v2/bson" 
 
type EventProcessor struct {
	EventListener messagequeue.EventListener
	Database      persistence.DatabaseHandler
}
func (p *EventProcessor) ProcessEvents() error {
	log.Println("Listening to events...")

	received, errors, err := p.EventListener.Listen("event.created")
	if err != nil {
		return err
	}

	for {
		select {
			case evt := <-received:
				p.handleEvent(evt)
			case err = <-errors:
				log.Printf("received error while processing msg: %s", err)
		}
	}
}
func (p *EventProcessor) handleEvent(event messagequeue.Event) {
	switch e := event.(type) { 
		case *mqcontracts.EventCreatedEvent:
			log.Printf("event %s created: %s", e.ID, e) 
			p.Database.AddEvent(persistence.Event{ID: bson.ObjectId(e.ID)}) 
		case *mqcontracts.LocationCreatedEvent:
			log.Printf("location %s created: %s", e.ID, e) 
			//p.Database.AddLocation(persistence.Location{ID: bson.ObjectId(e.ID)})
		default: 
			log.Printf("unknown event: %t", e) 
	} 
}