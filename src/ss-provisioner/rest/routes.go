package rest

import (
	"github.com/gorilla/mux"
	"net/http"
	"ss-lib/messagequeue"
	"ss-lib/persistence"
	"time"
)

func StartAPI(listenAddr string, database persistence.DatabaseHandler, eventEmitter messagequeue.EventEmitter) {
	r := mux.NewRouter()
	r.Methods("post").Path("/events/{eventID}/provision").Handler(&CreateProvisioningHandler{eventEmitter, database})

	srv := http.Server{
		Handler:      r,
		Addr:         listenAddr,
		WriteTimeout: 2 * time.Second,
		ReadTimeout:  1 * time.Second,
	}

	srv.ListenAndServe()
}